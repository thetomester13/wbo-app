#!/bin/bash

set -eu

mkdir -p /app/data/server-data

chown -R cloudron:cloudron /app/data/

echo "==> Starting WBO"
exec gosu cloudron:cloudron npm start

